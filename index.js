const express = require('express')

const app = express()
const cors = require('cors')

// 给app添加一个全局属性(用于秘钥)
app.set('secret', '1a3b5c7d9e')

// 解决跨域问题
app.use(cors())
app.use(express.json())

app.use('/upload/', express.static('./upload/'))

require('./plugins/db')(app)
require('./router/index')(app)

app.listen(5000, () => {
  console.log('http://localhost:5000/api/grain?')
})