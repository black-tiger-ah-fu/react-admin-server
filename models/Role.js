const mongoose = require('mongoose')
const shortid = require('shortid')

const schema = new mongoose.Schema({
  name: { type: String },
  create_tiem: { type: String },
  auth_time: { type: String },
  auth_name: { type: String },
  power: { type: Array, default: [] },
  key: { type: String, default: shortid() }
})

module.exports = mongoose.model('Role', schema)