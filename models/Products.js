const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  name: { type: String },
  status: { type: Boolean },
  imgs: [{ type: Object }],
  desc: { type: String },
  price: { type: Number },
  CategoryId: {
    type: mongoose.Types.ObjectId, 
    ref: 'Category'
  },
  detail: { type: String }
})

module.exports = mongoose.model('Products', schema)