const mongoose = require('mongoose')
const shortid = require('shortid')

const schema = new mongoose.Schema({
  username: {
    type: String,
    unique: true
  },
  password: {
    type: String
  },
  telephone: {
    type: String
  },
  register_tiem: {
    type: String
  },
  roleId: {
    type: mongoose.Types.ObjectId, 
    ref: 'Role'
  },
  key: {
    type: String,
    default: shortid()
  }
})

module.exports = mongoose.model('Login', schema)