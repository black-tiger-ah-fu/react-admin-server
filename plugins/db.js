module.exports = app => {
  const mongoose = require('mongoose')
  // 链接数据库
  mongoose.connect('mongodb://localhost/node-react-grain', {useNewUrlParser: true, useUnifiedTopology: true})

  require("require-all")(__dirname + '/../models')
}